## Developer Crypto License - DCL Token

The **Developer Crypto License**, grants legal rights to companies to use Open Source Code Libraries. Companies earn the legal right to use the software by **purchasing a DCL Token** which has to be embedded in the FOSS statements of their software products. The developers who release their code under the DCL license will get direct retribution every time a company uses their software by receiving a DCL Token. 


## Why Software Need it

Currently, the software communities create and publish software driven by passion or to create a portfolio, few of them manage to get support to continue the development and maintenance. The aim of the DCL Token is that developers are capable to find decentralized support for the release of their software. A lot of companies are using Open Source libraries without having the means to contribute to the development and maintenance of this software. For the companies is crucial having active communities maintaining the software they use. 

The use of the DCL token will boost the engagement of the software communities....


![Legal Format](https://www.dropbox.com/s/d17nndgoaa47cy7/IMG_C170EE831C40-1.jpeg?dl=0)

![Licenses in GitLab](https://www.dropbox.com/s/eqdre4xbg5tjnpb/Screenshot%202021-03-28%20at%2009.29.43.png?dl=0)

## How the DCL Network Works

When a company wants to use a Library will purchase a DCL token, to purchase the token will have to introduce the URL of the project, version and platform. The company will need to buy as many tokens as versions and platforms is using.

```
# The Token have encripted the following information.

Project_Name|Resitory_URL|Version|Platform|[Contributors_Profiles]
```

After the purchase is done the token is created and will to the developer a percentage of the value of the token. The remaining percetange will be given to the developer everytime he solves a bug for the current version. 

As the crytonetwork connects to the different git repositories, will know how to distribute the DCL tokens among the contributors of the project. 


## Boosting Software Developer Communities
